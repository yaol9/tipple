
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vuex from 'vuex'
import VueRouter from 'vue-router'
import store from './store'
import HomePage from './components/pages/HomePage.vue'
import ImportInvoicePage from './components/pages/ImportInvoicePage.vue'
import InvoiceDetailPage from './components/pages/InvoiceDetailPage.vue'

Vue.config.devtools = true
Vue.use(Vuex)
Vue.use(VueRouter)


const router = new VueRouter({
  mode: 'hash',
  base: __dirname,
  routes: [
    { path: '/', component: HomePage },
    { path: '/import', component: ImportInvoicePage },
    { path: '/view/:id', component: InvoiceDetailPage }
  ]
})


new Vue({
  router,
  store
}).$mount('#app');
