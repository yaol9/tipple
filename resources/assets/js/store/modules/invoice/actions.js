import * as types from '../../mutation-types'
import Vue from 'vue'

const actions = {
  loadInvoices({commit}) {
    axios.get('/api/invoices/')
        .then(function (response) {
          commit(types.LOAD_INVOICES, {
            invoices: response.data.invoices
          })
        })
        .catch(function (err) {
          console.log(err)  
        });
    
  },

  uploadInvoice({commit}, data) {
    axios.post('/api/invoices/', data)
        .then(function (response) {
          commit(types.NOFITY_UPLOADED_INVOICES, {
            uploadedInvoices: response.data.invoices
          })
        })
        .catch(function (err) {
          console.log(err)  
        });
    
  },

}

export default actions;