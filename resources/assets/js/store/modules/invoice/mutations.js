import * as types from '../../mutation-types'

const mutations = {
  [types.LOAD_INVOICES] (state, {invoices}) {
    state.invoices = invoices
  },

  [types.NOFITY_UPLOADED_INVOICES] (state, {uploadedInvoices}) {
    state.uploadedInvoices = uploadedInvoices
  },

}

export default mutations;