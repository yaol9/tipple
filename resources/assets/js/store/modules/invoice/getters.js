// state in module returns own state

const getters = {
  invoices(state) {
    return state.invoices
  },
}

export default getters;