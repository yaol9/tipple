<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta id="token" name="token" value="{ csrf_token() }">
        <title>Tipple code task</title>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">
        
        <!-- Styles -->
        <style>
            .content {
                text-align: center;
            }

            .title {
                font-size: 48px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            a {
                cursor: pointer;
            }
        </style>

    </head>
    <body>
        <div class="">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content" id="app">
                <div class="title m-b-md">
                    Invoice Management System
                </div>

                <router-view class="view"></router-view>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
