<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\InvoiceItem;
use Illuminate\Http\Request;
use Carbon\Carbon;
use XBase\Table;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $invoices = Invoice::with('items')->orderBy('id')->get();
        return response()->json([
            'invoices' => $invoices
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            //process DBF file
            $table = new Table($file->path() . $file->extension());
            $headLine = $table->nextRecord()->datahold;
            //echo $headLine."\n";
            $headLine = explode('    ', $headLine);
            $supplier = trim($headLine[0]);
            $invoiceDate = Carbon::createFromFormat('dmY', substr(trim($headLine[2]),0,8));
            $invoice = null;
            while ($record = $table->nextRecord()) {
                // $record->datahold holds the data
                if (strpos($record->datahold, 'INVOICING') !== false) {
                    // a new line of invoice
                    $invoice = new Invoice;
                    $invoice->supplier = $supplier;
                    $invoice->invoice_date = $invoiceDate;
                    $invoice->number = '';
                    $invoice->subtotal = 0.0;
                    $invoice->shrink_wrap = 0.0;
                    $invoice->admin_fee = 0.0;
                    $invoice->invoice_total = 0.0;
                } else if(strlen($record->datahold) === 200 && substr($record->datahold, 198, 2) === ' 0'){
                    // a line of invoice item
                    if (!$invoice->number) {
                        $invoice->number = substr($record->datahold, 2, 5);
                        $invoice->save();
                    }

                    // process a line of invoice item
                    $invoiceItem = new InvoiceItem;
                    $invoiceItem->invoice_id = $invoice->id;
                    $invoiceItem->product_code = trim(substr($record->datahold, 73, 6));
                    $invoiceItem->description = trim(substr($record->datahold, 41, 24));
                    $invoiceItem->size = trim(substr($record->datahold, 66, 5));
                    $invoiceItem->pack_qty = intval(trim(substr($record->datahold, 85, 4)));
                    $invoiceItem->order_qty = intval(trim(substr($record->datahold, 93, 7)));
                    $invoiceItem->delivery_qty = intval(trim(substr($record->datahold, 100, 7)));
                    $invoiceItem->base_cost = intval(trim(substr($record->datahold, 122, 8))) / 100;
                    $invoiceItem->delivery_fee = intval(trim(substr($record->datahold, 145, 4))) / 100;
                    $invoiceItem->case_cost = intval(trim(substr($record->datahold, 149, 7))) / 100;
                    $invoiceItem->total_cost = intval(trim(substr($record->datahold, 164, 8))) / 100;
                    $invoiceItem->total_gst = intval(trim(substr($record->datahold, 190, 8))) / 100;
                    $invoiceItem->save();
                } else if(strlen($record->datahold) === 200) {
                    // last line of each invoice
                    $invoice->subtotal = intval(trim(substr($record->datahold, 34, 9))) / 100;
                    $invoice->shrink_wrap = intval(trim(substr($record->datahold, 54, 9))) / 100;
                    $invoice->admin_fee = intval(trim(substr($record->datahold, 44, 9))) / 100;
                    $invoice->invoice_total = intval(trim(substr($record->datahold, 63, 9))) / 100;
                    $invoice->save();
                }
            }
            
        }
        // in future development this will be filled with the new invoices just been added
        $invoices = [];
        
        return response()->json([
            'invoices' => $invoices
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }
}
