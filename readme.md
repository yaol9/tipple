## About This Code Task

This code repository is for Tipple code task only. Please use Chrome/Firefox to check the work.

This code is using Laravel 5.4 on server side and for front-end part it's using VueJS 2.1. Check 'resources/assets/js/app.js' to see how VueJS has been setup. I use Vue-router to manage the navigation of different components, and use Vuex for state management.

## Deployment

The server need to be able to run Laravel 5.4 (requires PHP >= 5.6.4)
then run below scripts on server application folder after git pull

composer install --no-interaction --prefer-dist --optimize-autoloader
npm install
npm run production

No need to run db migration tasks as I am using a file db (sqlite) for this demo and I have added the db file in git repo as well. Of course in a real application we will use a more powerful db and db migration tasks need to be executed on every deployment.

To save your time an online demo has been installed here: http://128.199.190.109/

## Basic functions

To protect invoice data, login is required to use the online demo. I will send login detail in seperate email. 

The register page has been blocked so no more user can be registered. 

1. The demo has a index page to list all invoices.

2. On index page for each item there is a 'view' button. Click 'view' link to open the detail page which can display more details for this invoice.

3. User can upload new invoice DBF files by clicking 'Import Invoice(s)' button.

4. **this demo won't prevent you to upload a same invoice twice. In a real application we will check and alert if same invoice been submitted.**