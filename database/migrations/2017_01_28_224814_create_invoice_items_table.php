<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id');
            $table->string('product_code');
            $table->string('description');
            $table->string('size');
            $table->integer('pack_qty');
            $table->integer('order_qty');
            $table->integer('delivery_qty');
            $table->decimal('base_cost', 8, 2);
            $table->decimal('delivery_fee', 8, 2);
            $table->decimal('case_cost', 8, 2);
            $table->decimal('total_cost', 8, 2);
            $table->decimal('total_gst', 8, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
}
